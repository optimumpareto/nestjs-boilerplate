import { InconsistentModelError } from "@business/v1/errors/InconsistentModelError";
import { FuelType } from "@domain/cars/car.enums";
import { ICar, ICarAttached } from "@domain/cars/car.interface";
import { Car } from "@domain/cars/car.model";
import { IEngine } from "@domain/cars/engine/engine.interface";
import { Engine } from "@domain/cars/engine/engine.model";

/** Base unit tests for detached vs attached interface. DO_NOT_COPY */
describe('Any domain model', () => {
  const engine = new Engine("1.6 Turbo", FuelType.Gasoline, 140);
  
  describe('creating literal for detached interface', () => {
    const car: ICar = new Car("Fiat 500", engine, 40, 5 / 100);

    const carLiteral: ICar = {
      model: car.model, 
      avgFuelConsumption: car.avgFuelConsumption, 
      engine,
      fullTankCapacity: car.fullTankCapacity, 
      leftGas: car.leftGas,
      mileage: car.mileage, 
      manufacturingLineID: car.manufacturingLineID, 
      producedIn: car.producedIn
    };

    it('should equal model class created with constructor', () => {
      expect(carLiteral).toEqual(car);
    });
  });

  describe('creating literal for attached interface', () => {
    const car: ICar = new Car("Fiat 500", engine, 40, 5 / 100);

    const carAttached: ICarAttached = {
      model: car.model, 
      avgFuelConsumption: car.avgFuelConsumption, 
      engine: car.engine, 
      fullTankCapacity: car.fullTankCapacity, 
      leftGas: car.leftGas,
      mileage: car.mileage, 
      manufacturingLineID: car.manufacturingLineID, 
      producedIn: car.producedIn, 
      id: "Test_ID"
    };

    it('should result in non-empty id', () => {
      expect(carAttached.id).not.toBeUndefined();
    });
  });
});
