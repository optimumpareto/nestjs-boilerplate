import { InconsistentModelError } from "@business/v1/errors/InconsistentModelError";
import { FuelType } from "@domain/cars/car.enums";
import { ICar, ICarAttached } from "@domain/cars/car.interface";
import { Car } from "@domain/cars/car.model";
import { IEngine } from "@domain/cars/engine/engine.interface";
import { Engine } from "@domain/cars/engine/engine.model";
import { DefaultManufacturingLines } from "@domain/manufacturing-lines/manufacturingLine.defaults";

/**
 * Testing default constructor of Car class.  
 * Calling public constructor should always result in creating consistent object.
 * NOTE: These tests are not required for every model class. Use it only for testing complex constructor logic.
 */
describe('Car model', () => {
  const engine = new Engine("1.6 Turbo", FuelType.Gasoline, 140);
  
  describe('calling public constructor of Car class', () => {
    const car: ICar = new Car("Fiat 500", engine, 40, 5 / 100, 
    {
      manufacturingLineID: DefaultManufacturingLines.OptimumParetoMLine.id
    });

    it('should create a consistent object with proper model type', () => {
      expect(car.model).toEqual("Fiat 500");
    });

    it('should create a consistent object with proper engine type', () => {
      expect(car.engine).toEqual(engine);
    });

    it('should create a consistent object with 0 mileage', () => {
      expect(car.mileage).toEqual(0);
    });

    it('should create a consistent object with full tank', () => {
      expect(car.leftGas).toEqual(40);
    });
  });
});
