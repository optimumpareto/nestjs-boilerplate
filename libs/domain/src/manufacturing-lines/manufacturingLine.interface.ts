import { IBaseModelAttached } from "@domain/abstractions/attached.interface";

/** Represents a manufacturing line. */
export interface IManufacturingLine {
  /** An immutable name of the company. */
  readonly company: string;

  /** An immutable model of the manufactured car. */
  readonly model: string;

  /** An immutable limit of the number of cars that can be produced. */
  readonly manufacturingLimit: number;
}

/** Represents an attached manufacturing line. */
export type IManufacturingLineAttached = IManufacturingLine & IBaseModelAttached;