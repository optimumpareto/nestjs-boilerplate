import { IManufacturingLineAttached } from "./manufacturingLine.interface";

const OptimumParetoMLine: IManufacturingLineAttached = {
  id: "OptimumParetoMLine",
  company: "Optimum_Pareto", 
  model: "Optimum_Car", 
  manufacturingLimit: 100
};

const ReasonDrillMLine: IManufacturingLineAttached = {
  id: "ReasonDrillMLine",
  company: "Reason_Drill", 
  model: "RD_Car", 
  manufacturingLimit: 500
};

/** 
 * Represent default manufacturing lines which are not persisted in the database.
 */
export const DefaultManufacturingLines = {
  OptimumParetoMLine,
  ReasonDrillMLine
};