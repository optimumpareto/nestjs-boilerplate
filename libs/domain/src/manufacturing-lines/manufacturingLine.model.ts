import { IManufacturingLine } from "./manufacturingLine.interface";

/** 
 * Implements IManufacturingLine interface.
 * Adds default constructor which simplifies creation of detached IManufacturingLine models.
 */
export class ManufacturingLine implements IManufacturingLine {
  public constructor(
    company: string, 
    model: string, 
    manufacturingLimit: number,
  ) {
    //required parameters
    this.company = company;
    this.model = model;
    this.manufacturingLimit = manufacturingLimit;
  }

  readonly company: string;
  readonly model: string;
  readonly manufacturingLimit: number;
}