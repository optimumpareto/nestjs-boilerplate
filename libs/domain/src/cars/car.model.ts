import { InconsistentModelError } from "@business/v1/errors/InconsistentModelError";
import { DefaultManufacturingLines } from "@domain/manufacturing-lines/manufacturingLine.defaults";
import { FuelType } from "./car.enums";
import { ICar } from "./car.interface";
import { IEngine } from "./engine/engine.interface";

/** 
 * Implements ICar interface.
 * Adds default constructor which simplifies creation of detached ICar models.
 */
export class Car implements ICar {
  public constructor(
    model: string, 
    engine: IEngine, 
    fullTankCapacity: number, 
    avgFuelConsumption: number, 
    optionalProperties?: Partial<ICar>, 
  ) {
    //required parameters
    this.model = model;
    this.engine = engine;
    this.fullTankCapacity = fullTankCapacity;
    this.avgFuelConsumption = avgFuelConsumption;

    //the parameters which are optional or have default values
    this.manufacturingLineID = optionalProperties?.manufacturingLineID;
    this.mileage = optionalProperties?.mileage || 0;
    this.leftGas = optionalProperties?.leftGas || fullTankCapacity;
    this.producedIn = optionalProperties?.producedIn || [];
  }

  readonly manufacturingLineID?: string;
  readonly model: string;
  readonly engine: IEngine;
  readonly avgFuelConsumption: number;
  readonly fullTankCapacity: number;
  readonly producedIn: string[];

  leftGas: number;
  mileage: number;
}