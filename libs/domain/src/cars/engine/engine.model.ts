import { FuelType } from "../car.enums";
import { IEngine } from "./engine.interface";

/** 
 * Implements IEngine interface.
 * Adds default constructor which simplifies creation of detached IEngine models.
 */
export class Engine implements IEngine {
  public constructor(
    model: string,
    engineType: FuelType,
    horsePower: number,
  ) {
    //required parameters
    this.model = model;
    this.engineType = engineType;
    this.horsePower = horsePower;
  }

  readonly model: string;
  readonly engineType: FuelType;
  readonly horsePower: number;
}