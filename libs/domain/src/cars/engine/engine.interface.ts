import { FuelType } from "../car.enums";

/** Represents an engine. */
export interface IEngine {
  /** An immutable model of the engine. */
  readonly model: string;

  /** An immutable type of engine. */
  readonly engineType: FuelType;

  /** An immutable number of horse power. */
  readonly horsePower: number;
}
