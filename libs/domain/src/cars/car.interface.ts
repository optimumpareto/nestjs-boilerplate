import { IBaseModelAttached } from "@domain/abstractions/attached.interface";
import { FuelType } from "./car.enums";
import { IEngine } from "./engine/engine.interface";

/** Represents a car. */
export interface ICar {
  /** An immutable reference to manufacturing line. */
  readonly manufacturingLineID?: string;

  /** An immutable model of the car. */
  readonly model: string;

  /** An immutable type of engine. */
  readonly engine: IEngine;

  /** An immutable average fuel consumption. */
  readonly avgFuelConsumption: number;

  /** An immutable full tank capacity. */
  readonly fullTankCapacity: number;

  /** An immutable list of country IDs that this car is produced in. */
  readonly producedIn: string[];

  /** A mutable number of gallons left in the tank. */
  leftGas: number;

  /** A mutable mileage. */
  mileage: number;
}

/** Represents an attached car. */
export type ICarAttached = ICar & IBaseModelAttached;