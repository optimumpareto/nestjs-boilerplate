import { IBaseModelAttached } from "./attached.interface";

/**
 * A read-write repository for domain objects of type `T`.
 * @typeparam `T` A concrete type of the domain model.
 */
export interface IDomainRepository<T> extends
  IReadDomainRepository<T>,
  IWriteDomainRepository<T> {}

/**
 * A read-only repository for domain objects of type `T`.
 * @typeparam `T` A concrete type of the domain model.
 */
export interface IReadDomainRepository<T> {
  /** Returns all existing objects. */
  findAll(): Promise<Array<T & IBaseModelAttached>>;

  /** Finds an object by ID. Returns an attached object or undefined when object was not found. */
  findById(id: string): Promise<T & IBaseModelAttached | undefined>;

  /** 
   * Finds objects by specified criteria. Returns a list of attached objects. 
   * @param `criteria` A partial object of type `Optional<T>` 
   * containing the list of properties to search by. All properties have been made nullable.
   * 1. All conditions are `AND`ed. To use (OR) logic, run multiple searches.  
   * 2. Use `null`ed property to search by empty (or non-empty) value.
   * 3. Supports the following conditions for primitive types: 
   * `Equals`, `StartsWith`, `EndsWith`, `Contains`, `IsGreaterThan`, `IsLesserThan`
   * and the opposite (`NOT`) conditions. 
   * 4. Supports the following conditions for arrays: 
   * `Equals`, `Contains`, and the opposite (`NOT`) conditions. 
   * 5. Supports nested properties (2nd, 3rd and next level properties). 
   * @param `findOptions` Specifies a matching condition for each property. 
   * The default is: `IsExact`.
   */
  findByCriteria(criteria: Optional<T>, findOptions?: FindOptions<T>): Promise<Array<T & IBaseModelAttached>>;
}

/**
 * A write-only repository for domain objects of type `T`.
 * @typeparam `T` A concrete type of the domain model.
 */
export interface IWriteDomainRepository<T> {
  /** Creates a new object based on detached entity. Returns an attached object. */
  create(object: T): Promise<T & IBaseModelAttached>;

  /** Updates an attached object with a specific ID. */
  update(object: T & IBaseModelAttached): Promise<void>;

  /** Deletes an attached object. */
  delete(object: T & IBaseModelAttached): Promise<void>;
}

/** 
 * Makes all properties in T and all child object properties both optional 
 * and nullable for search purposes. 
 */
export type Optional<T> = { 
  [P in keyof T]?: 
    T[P] extends any[] ? T[P] | null : //array
    T[P] extends object ? Optional<T[P]> : //nested object
    T[P] | null; //primitive types
};

/** Specifies matching condition for each provided property. */
export type FindOptions<T> = {
  [P in keyof T]?: 
    T[P] extends any[] ? MatchingCondition : //array
    T[P] extends object ? FindOptions<T[P]> : //nested object
    MatchingCondition; //primitive types
};

/** A matching condition for criteria-based search. */
export enum MatchingCondition {
  NOT = -64,
  Equals = 1,
  StartsWith = 2,
  EndsWith = 4, 
  Contains = 8,
  IsGreaterThan = 16,
  IsLesserThan = 32, 
  //computed
  DoesNotEqual = NOT | Equals, 
  DoesNotStartWith = NOT | StartsWith, 
  DoesNotEndWith = NOT | EndsWith, 
  DoesNotContain = NOT | Contains, 
  LesserThanOrEqual = NOT | IsGreaterThan, 
  GreaterThanOrEqual = NOT | IsLesserThan, 
}
