import { IDomainRepository } from "@domain/abstractions/repository.interface";
import { Injectable } from "@apps/v1/node_modules/@nestjs/common";

/** TODO: insert logging logic */
// @Injectable()
// export class LoggingDecorator<T> implements IDomainRepository<T> {
//   constructor(private decoratee: IDomainRepository<T>) {}

//   findAll(): Promise<T[]> {
//     return this.decoratee.findAll();
//   }  
  
//   findById(id: string): Promise<T> {
//     return this.decoratee.findById(id);
//   }
  
//   create(object: T): Promise<T> {
//     return this.decoratee.create(object);
//   }
  
//   update(object: T): Promise<T> {
//     return this.decoratee.update(object);
//   }
  
//   delete(id: string): Promise<void> {
//     return this.decoratee.delete(id);
//   }
// }