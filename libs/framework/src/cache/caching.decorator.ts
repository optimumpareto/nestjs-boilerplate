import { Injectable } from "@apps/v1/node_modules/@nestjs/common";
import { BaseModel } from "@domain/abstractions/attached.interface";
import { IDomainRepository } from "@domain/abstractions/repository.interface";
import { CacheService } from "./cache.service";

/** This decorator automatically caches results of repository methods and updates cache when update method is invoked. */
@Injectable()
export class CachingDecorator<T extends BaseModel> implements IDomainRepository<T> {
  private typeName : string;

  constructor(
    x : T&Function,
    private readonly decoratee: IDomainRepository<T>, 
    private readonly cacheService: CacheService) {
      this.typeName = x.name;
    }

  private getCacheKeyForAllObjects = this.typeName + "_all";
  private getCacheKeyForObjectId = (id: string) => this.typeName + "_id_" + id;

  findAll(): Promise<T[]> {
    return this.cacheService.wrap<T[]>(this.getCacheKeyForAllObjects, () => this.decoratee.findAll());
  }  
  
  findById(id: string): Promise<T> {
    return this.cacheService.wrap<T>(this.getCacheKeyForObjectId(id), () => this.decoratee.findById(id));
  }
  
  async create(object: new () => Omit<T, 'id'>): Promise<T> {
    const attachedObject = await this.decoratee.create(object);
    this.cacheService.set(this.getCacheKeyForObjectId(attachedObject.id), attachedObject);
    return attachedObject;
  }
  
  async update(object: new () => T): Promise<T> {
    const attachedObject = await this.decoratee.update(object);
    this.cacheService.set(this.getCacheKeyForObjectId(attachedObject.id), attachedObject);
    return attachedObject;
  }
  
  async delete(id: string): Promise<void> {
    await this.decoratee.delete(id);
    this.cacheService.del(this.getCacheKeyForObjectId(id));
  }
}