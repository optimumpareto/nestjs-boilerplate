import { Injectable, CACHE_MANAGER, Inject } from '@apps/v1/node_modules/@nestjs/common';
import { ICacheOptions } from './models/cache.options';

@Injectable()
export class CacheService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager) {}
  
  get<T>(key: string): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      this.cacheManager.get(key, (err, result: T) => {
        if (err) 
          reject(err); 
        else 
          resolve(result);
      });
    });
  }
  
  set<T>(key: string, value: T, options?: ICacheOptions): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.cacheManager.set(key, value, options, (err) => {
        if (err) 
          reject(err); 
        else 
          resolve();
      });
    });
  }

  wrap<T>(key: string, runFunc: () => Promise<T>): Promise<T> {
    return new Promise<T>(async () => {
      let cachedValue: T = await this.get(key);
      if (cachedValue) 
        return cachedValue;

      cachedValue = await runFunc();
      this.set(key, cachedValue);

      return cachedValue;
    });
  }

  del(key: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.cacheManager.del(key, (err) => {
        if (err) 
          reject(err); 
        else 
          resolve();
      });
    });
  }
}