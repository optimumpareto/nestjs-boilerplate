import { Module, CacheModule } from '@apps/v1/node_modules/@nestjs/common';
import { CacheService } from './cache.service';

@Module({
  imports: [CacheModule.register()],
  controllers: [],
  providers: [CacheModule, CacheService],
  exports: [CacheService]
})

export class CustomCacheModule {}