import { Injectable } from "@apps/v1/node_modules/@nestjs/common";

@Injectable()
export class AuthConfig {
  authority: string = 'https://login.microsoftonline.com/b5e4f771-c0c7-4e37-90f6-d55402b5c172';
  issuer: string = 'https://sts.windows.net/b5e4f771-c0c7-4e37-90f6-d55402b5c172/';
  audience: string[] = ['0d81bc9d-5e49-4d7e-8906-62e9639723d8'];
}