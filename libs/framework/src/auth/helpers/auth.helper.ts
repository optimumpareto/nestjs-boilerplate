import { Injectable, HttpService, CACHE_MANAGER, Inject } from "@apps/v1/node_modules/@nestjs/common";
import * as jwt from 'jwt-simple';
import { User } from "@domain/users/user.model";
import { CacheService } from "@framework/cache/cache.service";
import { AuthConfig } from "../auth.config";
import { OpenIDCertificateKeys } from "../dto/openIDCertificateKeysDto";
import { OpenIDConfiguration } from "../dto/openIDConfigrationDto";
import { MicrosoftJwtTokenDto } from "../dto/microsoftJwtTokenDto";

@Injectable()
export class AuthHelper {
  constructor(
    private readonly httpService: HttpService, 
    private readonly authConfig: AuthConfig, 
    private readonly cacheService: CacheService) {}

  async decodeAndValidateToken (authorizationHeader: string): Promise<User | undefined> {
    try {
      const token = authorizationHeader.substring(7, authorizationHeader.length);
      const certificates = await this.getCertificates();

      certificates.forEach(async (publicKey) => {
        const decodedToken = jwt.decode(token, publicKey) as MicrosoftJwtTokenDto;
        if (this.validateToken(decodedToken)) {
            //TODO: get detailed user with group membership from userinfo endpoint  
            return new User();
        }
      });
    }
    catch (ex) {}
    
    return undefined;
  }

  validateToken (decodedToken: MicrosoftJwtTokenDto): boolean {
    return this.authConfig.issuer === decodedToken.iss 
        && this.authConfig.audience.indexOf(decodedToken.aud) >= 0 
        && new Date().getTime() / 1000 < decodedToken.exp 
        ? true
        : false;
  } 

  async getCertificates(): Promise<string[]> {
    const cachedCertificates = await this.cacheService.get<string>('AzureADCertificates');
    if (cachedCertificates) return cachedCertificates.split('~');
  
    const openIdConfigurationResponse = await this.httpService.get<OpenIDConfiguration>(
      this.authConfig.authority + "/.well-known/openid-configuration").toPromise();
  
    const certificatesResponse = await this.httpService.get<OpenIDCertificateKeys>(
      openIdConfigurationResponse.data.jwks_uri).toPromise();
      
    const convertedCertificates: string[] = [];
    certificatesResponse.data.keys.forEach(key => {
      key.x5c.forEach(keyItem => {
        convertedCertificates.push(this.convertCertificate(keyItem));
      });
    });

    await this.cacheService.set('AzureADCertificates', convertedCertificates.join('~'));

    return convertedCertificates;
  }
  
  convertCertificate = (cert: string) => {
    var beginCert = "-----BEGIN CERTIFICATE-----";
    var endCert = "-----END CERTIFICATE-----";
    
    cert = cert.replace("\n", "");
    cert = cert.replace(beginCert, "");
    cert = cert.replace(endCert, "");
    
    var result = beginCert;
    while (cert.length > 0) {
        
        if (cert.length > 64) {
            result += "\n" + cert.substring(0, 64);
            cert = cert.substring(64, cert.length);
        }
        else {
            result += "\n" + cert;
            cert = "";
        }
    }
    
    if (result[result.length ] != "\n")
        result += "\n";
  
    result += endCert + "\n";
    return result;
  }
}