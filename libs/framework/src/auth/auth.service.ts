import { Injectable } from '@apps/v1/node_modules/@nestjs/common';
import { Request } from 'express';
import { User } from '@domain/users/user.model';
import { AuthHelper } from './helpers/auth.helper';

@Injectable()
export class AuthService {
  constructor(private readonly authHelper: AuthHelper) {}
  
  async validateTokenIfPresent(request: Request): Promise<void> {
    if (request.headers.authorization) {
      const user = await this.authHelper.decodeAndValidateToken(request.headers.authorization);

      if (user) {
        request.isAuthenticated = () => true;
        request.user = user;
        return;
      }
    }

    request.isAuthenticated = () => false;
  }
}