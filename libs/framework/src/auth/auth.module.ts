import { Module, HttpModule } from '@apps/v1/node_modules/@nestjs/common';
import { CustomCacheModule } from '@framework/cache/cache.module';
import { AuthService } from './auth.service';
import { AuthMiddleware } from './auth.middleware';
import { AuthHelper } from './helpers/auth.helper';
import { AuthConfig } from './auth.config';

@Module({
  imports: [HttpModule, CustomCacheModule],
  controllers: [],
  providers: [AuthService, AuthMiddleware, AuthHelper, AuthConfig],
  exports: [AuthService, AuthMiddleware]
})

export class AuthModule {}