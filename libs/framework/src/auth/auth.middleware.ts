import { Injectable, NestMiddleware } from '@apps/v1/node_modules/@nestjs/common';
import { Request, Response } from 'express';
import { AuthService } from './auth.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly authService: AuthService) {}

  async use(request: Request, res: Response, next: Function) {
    await this.authService.validateTokenIfPresent(request);
    next();
  }
}
