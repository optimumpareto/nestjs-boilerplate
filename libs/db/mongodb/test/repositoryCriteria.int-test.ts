
import { CarEntity } from "@db/mongodb/cars/car.entity";
import { CarRepository } from "@db/mongodb/cars/car.repository";
import { MongoDbModule } from "@db/mongodb/mongo.db.module";
import { FindOptions, MatchingCondition } from "@domain/abstractions/repository.interface";
import { FuelType } from "@domain/cars/car.enums";
import { ICar, ICarAttached } from "@domain/cars/car.interface";
import { Car } from "@domain/cars/car.model";
import { Engine } from "@domain/cars/engine/engine.model";
import { Test, TestingModule } from "@nestjs/testing";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Connection, FindOperator, FindOptionsUtils } from "typeorm";

describe('CarRepository', () => {
  let createdCar: ICarAttached;
  let createdCar2: ICarAttached;
  let createdCar3: ICarAttached;
  const createdCars: ICarAttached[] = [];
  let carRepository: CarRepository;
  let connection: Connection;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        MongoDbModule,
        TypeOrmModule.forFeature([CarEntity])
      ],
      providers: [CarRepository],
    }).compile();

    carRepository = app.get<CarRepository>(CarRepository);
    connection = app.get<Connection>(Connection);

    createCars();
  });

  const createCars = async () => {
    createdCar = await carRepository.create(new Car(
      "Toyota Avensis", 
      new Engine("1.6 Turbo", FuelType.Gasoline, 140), 
      55, 8 / 100
    ));

    createdCar2 = await carRepository.create(new Car(
      "Peugeot 508", 
      new Engine("1.4", FuelType.Diesel, 115), 
      50, 6 / 100, 
      { 
        mileage: 100, 
        leftGas: 10, 
        manufacturingLineID: "-1",
        producedIn: ["1", "2"]
      }));

    createdCar3 = await carRepository.create(new Car(
      "Mazda CX5", 
      new Engine("2.0", FuelType.Gasoline, 180), 
      45, 9 / 100, 
      { 
        mileage: 200, 
        leftGas: 20, 
        manufacturingLineID: "1234", 
        producedIn: ["2", "3"]
      }));

    createdCars.push(createdCar);
    createdCars.push(createdCar2);
    createdCars.push(createdCar3);
  };
  
  afterAll(async () => {
    await Promise.all(createdCars.map(car => carRepository.delete(car)));
    //without this line jest does not exit properly
    connection.close(); 
  });

  //findByCriteria
  it('should find car by simple criteria', async () => {
    const foundCars1 = await carRepository.findByCriteria({ model: "Toyota Avensis" });
    expect(foundCars1.length).toBe(1);
    expect(foundCars1[0]).toEqual(createdCar);

    const foundCars2 = await carRepository.findByCriteria({ avgFuelConsumption: createdCar.avgFuelConsumption });
    expect(foundCars2.length).toBe(1);
    expect(foundCars2[0]).toEqual(createdCar);

    const foundCars3 = await carRepository.findByCriteria({ fullTankCapacity: createdCar.fullTankCapacity });
    expect(foundCars3.length).toBe(1);
    expect(foundCars3[0]).toEqual(createdCar);

    const foundCars4 = await carRepository.findByCriteria({ mileage: createdCar.mileage });
    expect(foundCars4.length).toBe(1);
    expect(foundCars4[0]).toEqual(createdCar);

    const foundCars5 = await carRepository.findByCriteria({ leftGas: createdCar.leftGas });
    expect(foundCars5.length).toBe(1);
    expect(foundCars5[0]).toEqual(createdCar);
  });

  it('should find car by child object property', async () => {
    const foundCars = await carRepository.findByCriteria(
      { engine: { horsePower: createdCar2.engine.horsePower } });

    expect(foundCars.length).toBe(1);
    expect(foundCars[0]).toEqual(createdCar2);
  });

  it('should find car by null property', async () => {
    const foundCars = await carRepository.findByCriteria({ manufacturingLineID: null });
    expect(foundCars.length).toBe(1);
    expect(foundCars[0]).toEqual(createdCar);
  });

  it('should NOT find specific car by undefined property, but rather return all cars', async () => {
    const foundCars = await carRepository.findByCriteria({ manufacturingLineID: undefined });
    expect(foundCars.length).toBe(createdCars.length);
  });

  it('should find car by two properties', async () => {
    const foundCars1 = await carRepository.findByCriteria({ 
      model: createdCar.model, 
      mileage: createdCar.mileage 
    });

    expect(foundCars1.length).toBe(1);
    expect(foundCars1[0]).toEqual(createdCar);

    const foundCars2 = await carRepository.findByCriteria({ 
      model: createdCar.model + "_xyz", 
      mileage: createdCar.mileage 
    });
    
    expect(foundCars2.length).toBe(0);

    const foundCars3 = await carRepository.findByCriteria({ 
      model: createdCar.model, 
      manufacturingLineID: null
    });

    expect(foundCars3.length).toBe(1);
    expect(foundCars3[0]).toEqual(createdCar);
  });

  it('should find multiple cars by DoesNotEqual condition', async () => {
    const foundCars = await carRepository.findByCriteria(
      { model: "Mazda CX5" }, 
      { model: MatchingCondition.DoesNotEqual } );
    expect(foundCars.length).toBe(2);
    expect(foundCars).toContainEqual(createdCar);
    expect(foundCars).toContainEqual(createdCar2);

    const foundCars2 = await carRepository.findByCriteria(
      { engine: { engineType: FuelType.Diesel } }, 
      { engine: { engineType: MatchingCondition.DoesNotEqual } } );
    expect(foundCars2.length).toBe(2);
    expect(foundCars2).toContainEqual(createdCar);
  });

  it('should find multiple cars by Contains and DoesNotContain condiction', async () => {
    const foundCars = await carRepository.findByCriteria(
      { model: "o" }, 
      { model: MatchingCondition.Contains });
    expect(foundCars.length).toBe(2);
    expect(foundCars).toContainEqual(createdCar);
    expect(foundCars).toContainEqual(createdCar2);

    const foundCars2 = await carRepository.findByCriteria(
      { model: "o" }, 
      { model: MatchingCondition.DoesNotContain });
    expect(foundCars2.length).toBe(1);
    expect(foundCars2).toContainEqual(createdCar3);
  });

  it('should find multiple cars by StartsWith condiction', async () => {
    const foundCars = await carRepository.findByCriteria(
      { model: "Toy" }, 
      { model: MatchingCondition.StartsWith });
    expect(foundCars.length).toBe(1);
    expect(foundCars).toContainEqual(createdCar);

    const foundCars2 = await carRepository.findByCriteria(
      { model: "Toy" }, 
      { model: MatchingCondition.DoesNotStartWith });
    expect(foundCars2.length).toBe(2);
    expect(foundCars2).toContainEqual(createdCar2);
    expect(foundCars2).toContainEqual(createdCar3);
  });

  it('should find multiple cars by EndsWith condiction', async () => {
    const foundCars = await carRepository.findByCriteria(
      { model: "508" }, 
      { model: MatchingCondition.EndsWith });
    expect(foundCars.length).toBe(1);
    expect(foundCars).toContainEqual(createdCar2);

    const foundCars2 = await carRepository.findByCriteria(
      { model: "508" }, 
      { model: MatchingCondition.DoesNotEndWith });
    expect(foundCars2.length).toBe(2);
    expect(foundCars2).toContainEqual(createdCar);
    expect(foundCars2).toContainEqual(createdCar3);
  });

  it('should find multiple cars by IsGreaterThan condiction', async () => {
    const foundCars = await carRepository.findByCriteria(
      { fullTankCapacity: 50 }, 
      { fullTankCapacity: MatchingCondition.IsGreaterThan });
    expect(foundCars.length).toBe(1);
    expect(foundCars).toContainEqual(createdCar);

    const foundCars2 = await carRepository.findByCriteria(
      { fullTankCapacity: 50 }, 
      { fullTankCapacity: MatchingCondition.LesserThanOrEqual });
    expect(foundCars2.length).toBe(2);
    expect(foundCars2).toContainEqual(createdCar2);
    expect(foundCars2).toContainEqual(createdCar3);
  });

  it('should find multiple cars by isLesserThan condiction', async () => {
    const foundCars = await carRepository.findByCriteria(
      { fullTankCapacity: 50 }, 
      { fullTankCapacity: MatchingCondition.IsLesserThan });
    expect(foundCars.length).toBe(1);
    expect(foundCars).toContainEqual(createdCar3);

    const foundCars2 = await carRepository.findByCriteria(
      { fullTankCapacity: 50 }, 
      { fullTankCapacity: MatchingCondition.GreaterThanOrEqual });
    expect(foundCars2.length).toBe(2);
    expect(foundCars2).toContainEqual(createdCar);
    expect(foundCars2).toContainEqual(createdCar2);
  });

  it('should find multiple cars by combined criteria', async () => {
    const foundCars = await carRepository.findByCriteria(
      { 
        mileage: 250,
        manufacturingLineID: null 
      }, 
      { 
        mileage: MatchingCondition.IsLesserThan, 
        manufacturingLineID: MatchingCondition.Equals 
      });
    expect(foundCars.length).toBe(1);
    expect(foundCars).toContainEqual(createdCar);

    const foundCars2 = await carRepository.findByCriteria(
      { 
        mileage: 100, 
        engine: { model: "1.4" } 
      }, 
      { 
        mileage: MatchingCondition.Equals, 
        engine: { model: MatchingCondition.Equals } 
      });
    expect(foundCars2.length).toBe(1);
    expect(foundCars2).toContainEqual(createdCar2);

    const foundCars3 = await carRepository.findByCriteria(
      { 
        avgFuelConsumption: 0, 
        manufacturingLineID: null 
      }, 
      { 
        avgFuelConsumption: MatchingCondition.GreaterThanOrEqual, 
        manufacturingLineID: MatchingCondition.DoesNotEqual 
      });
    expect(foundCars3.length).toBe(2);
    expect(foundCars3).toContainEqual(createdCar2);
    expect(foundCars3).toContainEqual(createdCar3);

    const foundCars4 = await carRepository.findByCriteria(
      { model: "not_existing" }, 
      { model: MatchingCondition.StartsWith });
    expect(foundCars4.length).toBe(0);
  });

  it('should find multiple cars by Array condiction', async () => {
    const foundCars = await carRepository.findByCriteria({ producedIn: [] });
    expect(foundCars.length).toBe(1);
    expect(foundCars).toContainEqual(createdCar);

    const foundCars2 = await carRepository.findByCriteria({ producedIn: ["1", "2"] });
    expect(foundCars2.length).toBe(1);
    expect(foundCars2).toContainEqual(createdCar2);

    const foundCars3 = await carRepository.findByCriteria(
      { producedIn: ["1", "2"] }, 
      { producedIn: MatchingCondition.DoesNotEqual }  
    );
    expect(foundCars3.length).toBe(2);
    expect(foundCars3).toContainEqual(createdCar);
    expect(foundCars3).toContainEqual(createdCar3);

    const foundCars4 = await carRepository.findByCriteria(
      { producedIn: ["1"] },
      { producedIn: MatchingCondition.Contains }  
    );
    expect(foundCars4.length).toBe(1);
    expect(foundCars4).toContainEqual(createdCar2);

    const foundCars5 = await carRepository.findByCriteria(
      { producedIn: ["1"] },
      { producedIn: MatchingCondition.DoesNotContain }  
    );
    expect(foundCars5.length).toBe(2);
    expect(foundCars5).toContainEqual(createdCar);
    expect(foundCars5).toContainEqual(createdCar3);

    const foundCars6 = await carRepository.findByCriteria(
      { producedIn: ["2"] },
      { producedIn: MatchingCondition.Contains }  
    );
    expect(foundCars6.length).toBe(2);
    expect(foundCars6).toContainEqual(createdCar2);
    expect(foundCars6).toContainEqual(createdCar3);

    const foundCars7 = await carRepository.findByCriteria(
      { producedIn: ["2"] },
      { producedIn: MatchingCondition.DoesNotContain }  
    );
    expect(foundCars7.length).toBe(1);
    expect(foundCars7).toContainEqual(createdCar);
  });
});
