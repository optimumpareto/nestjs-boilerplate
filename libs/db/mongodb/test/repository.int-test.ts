
import { CarEntity } from "@db/mongodb/cars/car.entity";
import { CarRepository } from "@db/mongodb/cars/car.repository";
import { MongoDbModule } from "@db/mongodb/mongo.db.module";
import { FuelType } from "@domain/cars/car.enums";
import { ICar, ICarAttached } from "@domain/cars/car.interface";
import { Car } from "@domain/cars/car.model";
import { Engine } from "@domain/cars/engine/engine.model";
import { Test, TestingModule } from "@nestjs/testing";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Connection } from "typeorm";

//mocked business method without error checks
const driveFor = (car: ICar, miles: number): void => {
  car.mileage += miles;
  car.leftGas -= miles * car.avgFuelConsumption;
};

//mocked business method without error checks
const refuel = (car: ICar, typeOfFule: FuelType, gallons: number): number => {
  car.leftGas += gallons;
  return car.leftGas;
};

describe('CarRepository', () => {
  const engine = new Engine("1.6 Turbo", FuelType.Gasoline, 140);

  const createdCars: ICarAttached[] = [];
  let carRepository: CarRepository;
  let connection: Connection;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        MongoDbModule,
        TypeOrmModule.forFeature([CarEntity])
      ],
      providers: [CarRepository],
    }).compile();

    carRepository = app.get<CarRepository>(CarRepository);
    connection = app.get<Connection>(Connection);
  });
  
  afterAll(async () => {
    await Promise.all(createdCars.map(car => carRepository.delete(car)));
    //without this line jest does not exit properly
    connection.close(); 
  });

  //create
  it('should insert new car', async () => {
    const cCreate = await carRepository.create(new Car("Fiat 500", engine, 40, 5 / 100));
    createdCars.push(cCreate);

    expect(cCreate.id).not.toBe(undefined);
    expect(cCreate.model).toBe("Fiat 500");
    expect(cCreate.fullTankCapacity).toEqual(40);
    expect(cCreate.avgFuelConsumption).toEqual(5 / 100);
  });

  //findAll
  it('should create two cars and return two cars', async () => {
    const cFindAll1 = await carRepository.create(new Car("Renault Megane", engine, 20, 7 / 100));
    const cFindAll2 = await carRepository.create(new Car("BMW 7", engine, 30, 8 / 100));

    createdCars.push(cFindAll1);
    createdCars.push(cFindAll2);

    const cars = await carRepository.findAll();

    expect(cars).toContainEqual(cFindAll1);
    expect(cars).toContainEqual(cFindAll2);
  });

  //findById
  it('should find car by id', async () => {
    const cFindByIdNewCar = new Car("Renault Megane", {
      model: "1.1", 
      engineType: FuelType.Diesel, 
      horsePower: 110
    }, 20, 7 / 100);

    const cFindById = await carRepository.create(cFindByIdNewCar);
    createdCars.push(cFindById);

    const foundcar = await carRepository.findById(cFindById.id);

    expect(foundcar).toEqual(cFindById);
    if (foundcar) expect(foundcar.engine).toEqual(cFindById.engine);
  });

  //update
  it('should update car', async () => {
    const cUpdate = await carRepository.create(new Car("Renault Megane", engine, 20, 7 / 100));
    createdCars.push(cUpdate);

    driveFor(cUpdate, 100);
    
    await carRepository.update(cUpdate);

    const foundCar = await carRepository.findById(cUpdate.id);

    if (foundCar) {
      expect(foundCar.mileage).toEqual(cUpdate.mileage);
      expect(foundCar.leftGas).toEqual(cUpdate.leftGas);
    }
  });

  //delete
  it('should delete car', async () => {
    const cDelete = await carRepository.create(new Car("Renault Megane", engine, 20, 7 / 100));

    await carRepository.delete(cDelete);

    const notFound = await carRepository.findById(cDelete.id);
    expect(notFound).toBeUndefined();
  });
});
