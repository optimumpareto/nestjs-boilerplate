import { filterPropertiesForSearchCriteria } from "@db/mongodb/abstractions/mapping.helper";

describe('filterPropertiesForSearchCriteria', () => {
  it('should remove undefined properties', () => {
    const input = {
      name: 'testname',
      firstname: undefined
    };

    const result = filterPropertiesForSearchCriteria(input);

    expect(result).toEqual({ 
      name: 'testname',
    });
  });

  it('should not remove null properties', () => {
    const input = {
      name: 'testname',
      firstname: null, 
      lastname: 'lastname'
    };

    const result = filterPropertiesForSearchCriteria(input);

    expect(result).toEqual({ 
      name: 'testname',
      firstname: null,
      lastname: 'lastname'
    });
  });

  it('should map first-level object', () => {
    const input = {
      name: 'testname',
      firstname: null, 
      lastname: 'lastname', 
      address: {
        country: 'poland',
        street: 'celna', 
        house: undefined, 
        postalCode: null
      }
    };

    const result = filterPropertiesForSearchCriteria(input);

    expect(result).toEqual({ 
      name: 'testname',
      firstname: null,
      lastname: 'lastname', 
      'address.country': 'poland',
      'address.street': 'celna',
      'address.postalCode': null
    });
  });

  it('should map second-level object', () => {
    const input = {
      name: 'testname',
      firstname: null, 
      lastname: 'lastname', 
      address: {
        country: 'poland',
        street: 'celna', 
        region: undefined, 
        postalCode: null, 
        house: {
          buildingNumber: 5, 
          flatNumber: undefined,
          doors: null
        }
      }
    };

    const result = filterPropertiesForSearchCriteria(input);

    expect(result).toEqual({ 
      name: 'testname',
      firstname: null,
      lastname: 'lastname', 
      'address.country': 'poland',
      'address.street': 'celna',
      'address.postalCode': null, 
      'address.house.buildingNumber': 5, 
      'address.house.doors': null, 
    });
  });
});