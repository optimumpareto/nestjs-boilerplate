import { FuelType } from "@domain/cars/car.enums";
import { Column } from "typeorm";

export class EngineEntity {
  @Column()
  model: string;

  @Column()
  engineType: FuelType;

  @Column()
  horsePower: number;
}