import { Column, Entity } from "typeorm";
import { BaseEntity } from "../abstractions/base.entity";
import { CarConsts } from "./car.consts";
import { EngineEntity } from "./sub-documents/engine.entity";

@Entity(CarConsts.CollectionName)
export class CarEntity extends BaseEntity {
  @Column()
  model: string;

  @Column(type => EngineEntity) //sub-document
  engine: EngineEntity;

  @Column()
  mileage: number;

  @Column()
  leftGas: number;

  @Column()
  fullTankCapacity: number;

  @Column()
  avgFuelConsumption: number;

  @Column()
  manufacturingLineID: string; //ref

  @Column()
  producedIn: string[];
}