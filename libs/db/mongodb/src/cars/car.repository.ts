
import { IDomainRepository, Optional } from "@domain/abstractions/repository.interface";
import { ICar, ICarAttached } from "@domain/cars/car.interface";
import { Car } from "@domain/cars/car.model";
import { IEngine } from "@domain/cars/engine/engine.interface";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { MongoRepository } from "typeorm";
import { BaseMongoRepository } from "../abstractions/base.repository";
import { CarEntity } from "./car.entity";
import { ObjectId } from "mongodb";

@Injectable()
export class CarRepository extends BaseMongoRepository<ICar, CarEntity>
implements IDomainRepository<ICar> {
  constructor(
    @InjectRepository(CarEntity)
    private readonly typeOrmCarRepository: MongoRepository<CarEntity>
  ) {
    super(typeOrmCarRepository);
  }

  protected async mapEntityToAttachedObject(entity: CarEntity): Promise<ICarAttached> {
    const carAttached: ICarAttached = {
      id: entity._id.toString(),
      model: entity.model, 
      engine: {
        model: entity.engine.model, 
        engineType: entity.engine.engineType, 
        horsePower: entity.engine.horsePower
      },
      fullTankCapacity: entity.fullTankCapacity, 
      avgFuelConsumption: entity.avgFuelConsumption, 
      manufacturingLineID: entity.manufacturingLineID, 
      mileage: entity.mileage,
      leftGas: entity.leftGas, 
      producedIn: entity.producedIn
    };

    return carAttached;
  }

  protected mapPartialObjectToEntity(object: Optional<ICarAttached>): Optional<CarEntity> {
    return {
      _id: object.id ? new ObjectId(object.id) : undefined,
      model: object.model, 
      engine: object.engine && {
        model: object.engine?.model, 
        engineType: object.engine?.engineType,
        horsePower: object.engine?.horsePower
      }, 
      fullTankCapacity: object.fullTankCapacity,
      avgFuelConsumption: object.avgFuelConsumption, 
      manufacturingLineID: object.manufacturingLineID, 
      leftGas: object.leftGas, 
      mileage: object.mileage, 
      producedIn: object.producedIn
    };
  }
}
