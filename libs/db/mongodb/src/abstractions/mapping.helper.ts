import { IBaseModelAttached } from "@domain/abstractions/attached.interface";
import { FindOptions, MatchingCondition } from "@domain/abstractions/repository.interface";
import { ObjectId } from "mongodb";
import { BaseEntity } from "./base.entity";

/** Filters undefined properties to construct valid search criteria for Mongo DB. */
export const filterPropertiesForSearchCriteria = (searchCriteria: object): unknown => {
  const filteredSearchCriteria = {};
  for (const key of Object.keys(searchCriteria)) {
    const keysAndValues = getKeysAndValues(key, searchCriteria[key]);
    keysAndValues.forEach(([k, v]) => filteredSearchCriteria[k] = v);
  }

  return filteredSearchCriteria;
};

/** Returns search keys for the provided property. */
const getKeysAndValues = (property: string, value: unknown): Array<[string, unknown]> => {
  //value is undefined
  if (value === undefined) {
    return [];
  }

  //value is a primitive type
  if (!(value instanceof Object) || Array.isArray(value)) {
    return [[property, value]];
  }

  const keysAndValues: Array<[string, unknown]> = [];

  //value is an object itself - we are searching by the property of a sub-document
  const embeddedObject = value as object;
  for (const internalKey of Object.keys(embeddedObject)) {
    //internal property value is undefined
    if (embeddedObject[internalKey] === undefined) {
      continue;
    }

    //primitive type of internal property
    if (!(embeddedObject[internalKey] instanceof Object)) {
      keysAndValues.push([property + "." + internalKey, embeddedObject[internalKey]]);
      continue;
    }

    const embeddedKeysAndValues = getKeysAndValues(internalKey, embeddedObject[internalKey]);
    embeddedKeysAndValues.forEach(([k, v]) => {
      keysAndValues.push([property + "." + k, v]);
    });
  }

  return keysAndValues;
};

const convertFindOptions = <T>(findOptions?: FindOptions<T>): FindOptions<T> | undefined => {
  if (!findOptions) return findOptions;

  const findOptionsConverted = {};
  for (const key of Object.keys(findOptions)) {
    const keysAndValues = getKeysAndValues(key, findOptions[key]);
    keysAndValues.forEach(([k, v]) => findOptionsConverted[k] = v);
  }

  return findOptionsConverted;
};

/** Creates complex search criteria for Mongo DB. */
export const mapComplexCriteria = <T>(criteria: any, findOptions?: FindOptions<T>): unknown => {
  const convertedFindOptions = convertFindOptions(findOptions);
  const advancedCriteria = changeArrayValuesToMongoCriteria(criteria, convertedFindOptions);
  const fullCriteriaObject = { where: { $and: advancedCriteria } };
  return fullCriteriaObject;
};

const changeArrayValuesToMongoCriteria = <T>(obj: any, findOptions?: FindOptions<T>): any[] => {
  const allCriterions: unknown[] = [];

  for (const key of Object.keys(obj)) {
    const partialFC = {};

    //array property
    if (obj[key] instanceof Array) {
      if (!findOptions || !findOptions[key] || findOptions[key] === MatchingCondition.Equals) {
        partialFC[key] = { $eq: obj[key] };
        allCriterions.push(partialFC);
        continue;
      }

      switch (findOptions[key]) {
        case MatchingCondition.DoesNotEqual: 
          partialFC[key] = { $ne: obj[key] }; 
          break;
        case MatchingCondition.Contains: 
          partialFC[key] = { $in: obj[key] }; 
          break;
        case MatchingCondition.DoesNotContain: 
          partialFC[key] = { $nin: obj[key] }; 
          break;
        default: 
          throw new Error(`The matching condition ${findOptions[key]} cannot be used with arrays.`);
      }

      allCriterions.push(partialFC);
      continue;
    } 

    //default equals
    if (!findOptions || !findOptions[key])  {
      partialFC[key] = { $eq: obj[key] };
      allCriterions.push(partialFC);
      continue;
    }

    switch (findOptions[key]) {
      case MatchingCondition.Equals: 
        partialFC[key] = { $eq: obj[key] }; 
        break;
      case MatchingCondition.StartsWith: 
        partialFC[key] = { $regex: new RegExp('^' + obj[key] + '.*$') }; 
        break;
      case MatchingCondition.EndsWith: 
        partialFC[key] = { $regex: new RegExp(obj[key] + '$') }; 
        break;
      case MatchingCondition.Contains: 
        partialFC[key] = { $regex: new RegExp('^.*' + obj[key] + '.*$') }; 
        break;
      case MatchingCondition.IsGreaterThan: 
        partialFC[key] = { $gt: obj[key] }; 
        break;
      case MatchingCondition.IsLesserThan: 
        partialFC[key] = { $lt: obj[key] }; 
        break;
      ////////////////////////////////////
      case MatchingCondition.DoesNotEqual: 
        partialFC[key] = { $ne: obj[key] };
        break;
      case MatchingCondition.DoesNotStartWith: 
        partialFC[key] = { $not: new RegExp('^' + obj[key] + '.*$') };
        break;
      case MatchingCondition.DoesNotEndWith: 
        partialFC[key] = { $not: new RegExp(obj[key] + '$') };
        break;
      case MatchingCondition.DoesNotContain: 
        partialFC[key] = { $not: new RegExp('^.*' + obj[key] + '.*$') };
        break;
      case MatchingCondition.GreaterThanOrEqual: 
        partialFC[key] = { $gte: obj[key] };
        break;
      case MatchingCondition.LesserThanOrEqual: 
        partialFC[key] = { $lte: obj[key] };
        break;
    }

    allCriterions.push(partialFC);
  }

  return allCriterions;
};