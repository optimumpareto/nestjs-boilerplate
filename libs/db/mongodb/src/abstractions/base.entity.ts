import { ObjectId } from "mongodb";
import { Column, ObjectIdColumn } from "typeorm";

/**
 * A base class for each entity persisted as a collection document in the Mongo DB database.
 */
export class BaseEntity {
  /** A MongoDb ID of the object. */
  @ObjectIdColumn()
  // tslint:disable-next-line: variable-name
  readonly _id: ObjectId;
}
