import { IBaseModelAttached } from "@domain/abstractions/attached.interface";
import { FindOptions, IDomainRepository, Optional } from "@domain/abstractions/repository.interface";
import { ObjectId } from "mongodb";
import { MongoRepository } from "typeorm";
import { BaseEntity } from "./base.entity";
import { filterPropertiesForSearchCriteria, mapComplexCriteria } from "./mapping.helper";

/**
 * A generic implementation of entity repository.
 * @typeparam `T` is type of domain object.
 * @typeparam `E` is type of DB entity.
 */
export abstract class BaseMongoRepository<T, E extends BaseEntity>
implements IDomainRepository<T> {
  constructor(private readonly typeOrmDRepository: MongoRepository<E>) {}

  /** Remember about mapping reference properties! */
  protected abstract mapEntityToAttachedObject(entity: E): Promise<T & IBaseModelAttached>;

  /** Remember about using helper methods from `base.mapper`! */
  protected abstract mapPartialObjectToEntity(object: Optional<T & IBaseModelAttached>): Optional<E>;

  ///////////////////////////////////////////////////////////////////////////

  /** Finds all entities of type `E` and maps them to objects of type `T`. */
  async findAll(): Promise<Array<T & IBaseModelAttached>> {
    const allEntities = await this.typeOrmDRepository.find();
    return Promise.all(allEntities.map(e => this.mapEntityToAttachedObject(e)));
  }

  /** Finds an entity of type `E` by ID and maps it to object of type `T`. */
  async findById(id: string): Promise<T & IBaseModelAttached | undefined> {
    const foundEntity = await this.typeOrmDRepository.findOne(id);
    return foundEntity ? this.mapEntityToAttachedObject(foundEntity) : undefined;
  }

  /** Finds entities of type `E` by custom criteria and maps them to objects of type `T`. */
  async findByCriteria(criteria: Optional<T & IBaseModelAttached>, findOptions?: FindOptions<T>)
  : Promise<Array<T & IBaseModelAttached>> {
    const mappedProperties = this.mapPartialObjectToEntity(criteria);
    const filteredCriteria = filterPropertiesForSearchCriteria(mappedProperties);

    let mappedCriteria: any = filteredCriteria;
    //if at least one criteria property is an array or 
    //additional options are requested - use complex criteria mapping
    if (findOptions || Object.keys(criteria).find(k => Array.isArray(criteria[k]))) {
      mappedCriteria = mapComplexCriteria(filteredCriteria, findOptions);
    }

    const foundEntities = await this.typeOrmDRepository.find(mappedCriteria);
    return Promise.all(foundEntities.map(e => this.mapEntityToAttachedObject(e)));
  }

  /**
   * Takes a new detached object of type `T`, converts it to detached entity of type `E`
   * and inserts it into database.
   */
  async create(object: T): Promise<T & IBaseModelAttached> {
    const mappedEntity = this.mapPartialObjectToEntity(object as Optional<T & IBaseModelAttached>);
    const result = await this.typeOrmDRepository.insertOne(mappedEntity);
    return this.mapEntityToAttachedObject(result.ops[0]);
  }

  /** Converts an attached object of type `T` to entity of type `E` and updates it. */
  async update(object: T & IBaseModelAttached): Promise<void> {
    const mappedEntity = this.mapPartialObjectToEntity(object as Optional<T & IBaseModelAttached>);
    const updateResult = await this.typeOrmDRepository.replaceOne({ _id: mappedEntity._id }, mappedEntity);
    return updateResult.modifiedCount === 1 ? Promise.resolve() : Promise.reject();
  }

  /** Deletes an attached object. */
  async delete(object: T & IBaseModelAttached): Promise<void> {
    const deleteResult = await this.typeOrmDRepository.deleteOne({ _id: new ObjectId(object.id) });
    return deleteResult.deletedCount === 1 ? Promise.resolve() : Promise.reject();
  }
}
