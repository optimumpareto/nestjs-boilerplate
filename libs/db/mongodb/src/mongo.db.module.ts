import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CarEntity } from "./cars/car.entity";
import { CarRepository } from "./cars/car.repository";

const globalORMConfig = TypeOrmModule.forRoot({
  type: 'mongodb',
  host: 'localhost',
  port: 27017,
  database: 'local',
  entities: [
    CarEntity
  ],
  useUnifiedTopology: true, 
  keepConnectionAlive: true
});

@Module({
  imports: [globalORMConfig],
  controllers: [],
  providers: [],
  exports: [],
})

export class MongoDbModule {}