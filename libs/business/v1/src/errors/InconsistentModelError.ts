export class InconsistentModelError extends Error {
  constructor(message: string) {
    super(message);
  }
}