export class ValidationError extends Error {
  public type: string;
  public propertyName: string;

  constructor(type: ValidationErrorType, propertyName: string, explanation?: string) {
    super(getErrorMessage(type, propertyName, explanation));
    this.type = type;
    this.propertyName = propertyName;
  }
}

export enum ValidationErrorType {
  NON_EMPTY_PROPERTY = "NON_EMPTY_PROPERTY", 
  EMPTY_PROPERTY = "EMPTY_PROPERTY",
  INVALID_VALUE = "INVALID_VALUE",  
}

export const getErrorMessage = (type: ValidationErrorType, propertyName: string, explanation?: string): string => {
  const explanationToAppend = explanation ? " " + explanation : "";

  switch (type) {
    case(ValidationErrorType.NON_EMPTY_PROPERTY):
      return `The property ${propertyName} must be empty.${explanationToAppend}`;
    case(ValidationErrorType.EMPTY_PROPERTY):
      return `The property ${propertyName} cannot be empty.${explanationToAppend}`;
    case(ValidationErrorType.INVALID_VALUE):
      return `The property ${propertyName} has invalid value.${explanationToAppend}`;
  }
};
