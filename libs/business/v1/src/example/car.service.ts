import { FindOptions, IDomainRepository, MatchingCondition } from "@domain/abstractions/repository.interface";
import { FuelType } from "@domain/cars/car.enums";
import { Car } from "@domain/cars/car.model";
import { Inject, Injectable } from "@nestjs/common";
import { UnsupportedFeatureError } from "../errors/UnsupportedFeatureError";
import { ValidationError, ValidationErrorType } from "../errors/ValidationError";
import { NewCarDto } from "./dto/newCar.dto";

@Injectable()
export class CarService {
  constructor(
    @Inject('CarRepository') private readonly _carRepository: IDomainRepository<Car>,
  ) {}

  async createNew(newCarDto: NewCarDto, manufacturingLineID?: string): Promise<Car> {
    if (newCarDto.engine.engineType === FuelType.Diesel) {
      throw new UnsupportedFeatureError("Diesel engines are not supported in the current model.");
    }

    if (newCarDto.fullTankCapacity > 80) {
      throw new ValidationError(
        ValidationErrorType.INVALID_VALUE, 
        "fullTankCapacity", 
        "Cannot create a car with tank larger than 80 litres.");
    }

    const newCar = new Car(
      newCarDto.model, 
      newCarDto.engine, 
      newCarDto.fullTankCapacity, 
      10 / 100, 
      { 
        manufacturingLineID
      });

    const createdCar = await this._carRepository.create(newCar);
    return createdCar;
  }

  async getNumberOfCreatedVolvos(): Promise<number> {
    const foundCars = await this._carRepository.findByCriteria(
      { model: "Volvo" }, 
      { model: MatchingCondition.StartsWith 
    });
    
    return foundCars.length;
  }
}