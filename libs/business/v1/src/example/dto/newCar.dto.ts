import { Engine } from "@domain/cars/engine/engine.interface";

export class NewCarDto {
  model: string; 
  fullTankCapacity: number;
  engine: Engine;
}