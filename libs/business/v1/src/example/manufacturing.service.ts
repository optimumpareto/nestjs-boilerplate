import { IDomainRepository } from "@domain/abstractions/repository.interface";
import { FuelType } from "@domain/cars/car.enums";
import { Engine } from "@domain/cars/engine/engine.interface";
import { ManufacturingLine } from "@domain/manufacturing-lines/manufacturingLine.interface";
import { Inject, Injectable } from "@nestjs/common";
import { CarService } from "./car.service";

@Injectable()
export class ManufacturingService {
  constructor(
    @Inject('CarService') 
    private readonly _carService: CarService,
    @Inject('ManufacturingLineRepository') 
    private readonly _manufacturingLineRepository: IDomainRepository<ManufacturingLine>,
  ) {}

  async manufactureNewVolvo(): Promise<void> {
    const mLines = await this._manufacturingLineRepository.findByCriteria({company: "Volvo"});
    const mLine = mLines[0];

    const numberOfCreatedVolvos = await this._carService.getNumberOfCreatedVolvos();
    if (numberOfCreatedVolvos > mLine.manufacturingLimit) {
      throw new Error(`Cannot manufacture more than ${mLine.manufacturingLimit} cars!`);
    }

    await this._carService.createNew({
      model: mLine.company + " " + mLine.model,
      engine: new Engine("2.0 Turbo", FuelType.Gasoline, 160),
      fullTankCapacity: 600 
    }, mLine.id);
  }
}