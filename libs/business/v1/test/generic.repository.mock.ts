import { BaseModel } from "@domain/abstractions/attached.interface";
import { FindOptions, IDomainRepository } from "@domain/abstractions/repository.interface";

export class GenericRepositoryMock<T extends BaseModel> implements IDomainRepository<T> {
  async findAll(): Promise<T[]> {
    return [];
  }  
  
  async findById(id: string): Promise<T | undefined> {
    return undefined;
  }

  async findByCriteria(criteria: Partial<T>, findOptions?: FindOptions<T>): Promise<T[]> {
    return [];
  }

  async create(object: T): Promise<T> {
    return object;
  }

  async update(object: T): Promise<void> {
    return;
  }

  async delete(object: T): Promise<void> {
    return;
  }
}
