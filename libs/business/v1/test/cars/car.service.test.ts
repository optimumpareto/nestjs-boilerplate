import { UnsupportedFeatureError } from "@business/v1/errors/UnsupportedFeatureError";
import { ValidationError, ValidationErrorType } from "@business/v1/errors/ValidationError";
import { CarService } from "@business/v1/example/car.service";
import { NewCarDto } from "@business/v1/example/dto/newCar.dto";
import { FuelType } from "@domain/cars/car.enums";
import { Car } from "@domain/cars/car.model";
import { Engine } from "@domain/cars/engine/engine.interface";
import { GenericRepositoryMock } from "../generic.repository.mock";

//Test-Driven Development (TDD)
describe('CarService', () => {
  const mockedCarRepository = new GenericRepositoryMock<Car>();
  const carService = new CarService(mockedCarRepository);

  describe('calling createCar method', () => {
    it('should throw unsupported feature error when creating a car with diesel engine', () => {
      expect(carService.createNew({
        model: "Volvo VX 70", 
        fullTankCapacity: 60, 
        engine: new Engine("2.0 Turbo", FuelType.Diesel, 220)
      }))
      .rejects.toEqual(new UnsupportedFeatureError("Diesel engines are not supported in the current model."));
    });

    it('should throw validation error when creating a car with too large tank.', () => {
      const unsupportedEngine = new Car("Volvo", new Engine("2.0 Turbo", FuelType.Gasoline, 220), 600, 7 / 100);
      expect(carService.createNew(unsupportedEngine))
      .rejects.toEqual(new ValidationError(
        ValidationErrorType.INVALID_VALUE, 
        "fullTankCapacity", 
        "Cannot create a car with tank larger than 80 litres."));
    });

    it('should invoke repository create method once when creating a car with gasoline engine', async () => {
      const spiedCreateMethod = jest.spyOn(mockedCarRepository, 'create')
        .mockImplementation(() => Promise.resolve({} as Car));

      const unsupportedEngine: NewCarDto = {
        model: "Volvo", 
        engine: new Engine("2.0 Turbo", FuelType.Gasoline, 220),  
        fullTankCapacity: 60
      };

      await carService.createNew(unsupportedEngine);

      //alternative check
      expect(mockedCarRepository.create).toHaveBeenCalledTimes(1); 
      //recommended
      expect(spiedCreateMethod.mock.calls[0][0].avgFuelConsumption).toEqual(10 / 100);
      expect(spiedCreateMethod.mock.calls[0][0].model).toEqual("Volvo");
      expect(spiedCreateMethod.mock.calls[0][0].engine.model).toEqual("2.0 Turbo");
    });
  });
});
