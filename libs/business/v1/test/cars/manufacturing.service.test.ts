import { CarService } from "@business/v1/example/car.service";
import { ManufacturingService } from "@business/v1/example/manufacturing.service";
import { Car } from "@domain/cars/car.model";
import { ManufacturingLine } from "@domain/manufacturing-lines/manufacturingLine.interface";
import { GenericRepositoryMock } from "../generic.repository.mock";

//Test-Driven Development (TDD)
describe('ManufacturingLine', () => {
  const mockedCarRepository = new GenericRepositoryMock<Car>();
  const mockedMLRepository = new GenericRepositoryMock<ManufacturingLine>();
  const carService = new CarService(mockedCarRepository);
  const mlService = new ManufacturingService(carService, mockedMLRepository);

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('calling manufactureVolvo method', () => {
    it('should manufacture new Volvo', async () => {
      const spiedCreateNewMethod = jest.spyOn(carService, 'createNew')
      .mockImplementation(() => Promise.resolve({} as Car));

      jest.spyOn(carService, 'getNumberOfCreatedVolvos').mockImplementation(() => Promise.resolve(105));

      const spiedFindByCriteriaMethod = jest.spyOn(mockedMLRepository, 'findByCriteria')
      .mockImplementation(() => Promise.resolve([new ManufacturingLine("Volvo", "XC90", 200, {
        id: "1234", 
        createdAt: new Date(),
        lastUpdatedAt: new Date()
      })]));

      await mlService.manufactureNewVolvo();

      expect(spiedFindByCriteriaMethod.mock.instances.length).toEqual(1);
      expect(spiedCreateNewMethod.mock.instances.length).toEqual(1);
      expect(spiedCreateNewMethod.mock.calls[0][0].model).toEqual("Volvo XC90");
      expect(spiedCreateNewMethod.mock.calls[0][1]).toEqual("1234");
    });

    it('should not manufacture new Volvo when reached manufacturing limit', async () => {
      const spiedCreateNewMethod = jest.spyOn(carService, 'createNew')
      .mockImplementation(() => Promise.resolve({} as Car));
      
      const spiedFindAllCreatedVolvos = jest.spyOn(carService, 'getNumberOfCreatedVolvos')
      .mockImplementation(() => Promise.resolve(105));
      
      jest.spyOn(mockedMLRepository, 'findByCriteria')
      .mockImplementation(() => Promise.resolve([new ManufacturingLine("Volvo", "XC90", 100, {
        id: "1234", 
        createdAt: new Date(),
        lastUpdatedAt: new Date()
      })]));

      //ASYNC METHODS SHOULD BE INVOKED AS: EXPECT(ASYNC_METHOD()).REJECTS
      await expect(mlService.manufactureNewVolvo()).rejects.toEqual(new Error("Cannot manufacture more than 100 cars!"));
      expect(spiedFindAllCreatedVolvos.mock.instances.length).toEqual(1);
      expect(spiedCreateNewMethod.mock.instances.length).toEqual(0);
    });
  });
});
