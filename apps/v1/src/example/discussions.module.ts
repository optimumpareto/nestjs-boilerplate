import { Module } from '@nestjs/common';
import { DiscussionsController } from './discussions.controller';
import { DiscussionsService } from './discussions.service';
import { DiscussionsDbModule } from 'libs/db/mongodb/src/discussions/discussions.db.module';
import { DiscussionRepository } from 'libs/db/mongodb/src/discussions/discussion.repository';

const discussionsRepositoryProvider = {
  provide: 'IDomainRepository',
  useClass: DiscussionRepository,
};

@Module({
  imports: [DiscussionsDbModule],
  controllers: [DiscussionsController],
  providers: [discussionsRepositoryProvider, DiscussionRepository, DiscussionsService],
})

export class DiscussionsModule {}
