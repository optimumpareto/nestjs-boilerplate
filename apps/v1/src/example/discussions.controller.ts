import { Controller, Get, Param, Post, Body, Put, Delete, HttpStatus, HttpException, ForbiddenException } from '@nestjs/common';
import { Discussion } from '@domain/discussions/discussion.model';
import { DiscussionsService } from './discussions.service';
import { CreateDiscussionDto } from '../../../../libs/business/v1/src/discussions/dto/createDiscussion.dto';
import { UpdateDiscussionDto } from '../../../../libs/business/v1/src/discussions/dto/updateDiscussion.dto';

@Controller('discussions')
export class DiscussionsController {
  constructor(private readonly discussionService: DiscussionsService) {}

  // @Get()
  // findAll(): Promise<Discussion[]> {
  //   return this.discussionService.findAll();
  // }

  // @Get(':discussionId')
  // findById(@Param('discussionId') id: number): Promise<Discussion> {
  //   return this.discussionService.findById(id);
  // }

  // @Post()
  // create(@Body() createDiscussionDto: CreateDiscussionDto): Promise<Discussion> {
  //   return this.discussionService.create(createDiscussionDto);
  // }

  // @Put(':discussionId')
  // update(@Param('discussionId') id: number, @Body() updateDiscussionDto: UpdateDiscussionDto): Promise<void> {
  //   return this.discussionService.update(updateDiscussionDto);
  // }

  // @Delete(':discussionId')
  // remove(@Param('discussionId') id: number): Promise<void> {
  //   return this.discussionService.remove(id);
  // }
}
