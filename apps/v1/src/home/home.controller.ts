import { Controller, Get } from '@nestjs/common';

@Controller()
export class HomeController {
  constructor() {}

  @Get()
  getHello(): string {
    return "Welcome to Swarmcheck v1-Templates API";
  }
}
