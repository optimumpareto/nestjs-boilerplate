import { Module, ValidationPipe, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { HomeController } from './home/home.controller';
import { DiscussionsModule } from './example/discussions.module';
import { APP_PIPE, APP_FILTER, BaseExceptionFilter } from '@nestjs/core';
import { AuthModule } from '../../../libs/framework/src/auth/auth.module';
import { AuthMiddleware } from '../../../libs/framework/src/auth/auth.middleware';

@Module({
  imports: [DiscussionsModule, AuthModule],
  controllers: [HomeController],
  providers: [
    // {
    //   provide: APP_FILTER,
    //   useClass: BaseExceptionFilter,
    // },
    // {
    //   provide: APP_PIPE,
    //   useClass: ValidationPipe,
    // },
  ],
})

export class AppModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}