import "reflect-metadata";
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.enableCors(corsOptions);

  await app.listen(3001);
}

bootstrap();
