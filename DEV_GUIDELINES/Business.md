# Business

Business logic should be implemented using Test-Driven Development (TDD) approach.

![alt text](./images/Layered_Architecture_Business.png)

**Business project will contain**:
- **business services**
- implemented requirements
- validation logic
- custom errors (optional)
- unit-tests with business logic

## 1. Business logic:

- depends on general domain model
- represents real business rules
- implements **concrete requirements**
- corresponds to use cases and user stories
- is implemented for each product version 

**Relevant SOLID principles**: 
- Single responsibility principle
  *"A class should only have a single responsibility, that is, only changes to one part of the software's specification should be able to affect the specification of the class."*

## 2. Test-Driven Design (TDD)

Steps: 
1. Create new specific business service. Add required model repositories as parameters.

```typescript
@Injectable()
export class CarService {
  constructor(
    @Inject('CarRepository') private readonly _carRepository: IDomainRepository<Car>,
  ) {}
}
```

2. Create a folder called **dto** with Data-Tranferred-Objects. Those classes will represent input and output of your business logic functions.

```typescript
export class NewCarDto {
  model: string; 
  fullTankCapacity: number;
  engine: Engine;
}
```

3. Create new unit test and add your business logic: expected input and output. 
For example:

```typescript
export class NewCarDto {
  model: string; 
  fullTankCapacity: number;
  engine: Engine;
}
```

4. Create a failing test

*This is how business describes the conditions and expected results.*

```typescript
  describe('calling createCar method', () => {
    it('should throw unsupported feature error when creating a car with diesel engine', () => {
      expect(carService.createNew({
        model: "Volvo VX 70", 
        fullTankCapacity: 60, 
        engine: new Engine("2.0 Turbo", FuelType.Diesel, 220)
      }))
      .rejects.toEqual(new UnsupportedFeatureError("Diesel engines are not supported in the current model."));
    });
  }
```

5. Run Test, confirm it <font color="red">fails</font>.

6. Work on method implementation until the test <font color="green">passes</font>.

*This is how programmers implement business requirements*.

```typescript
async createNew(newCarDto: NewCarDto, manufacturingLineID?: string): Promise<Car> {
    if (newCarDto.engine.engineType === FuelType.Diesel) {
      throw new UnsupportedFeatureError("Diesel engines are not supported in the current model.");
    }
    [...]
}
```

**To remember**: the test describes business logic better than the implementation itself.

## 3. DTO models

#### Business service actions can take input parameters as:
a) nothing, there are no input parameters
- there are not many methods which take no input parameters
- do not pass currentUserId by parameter, it can be read from 
`userService.getCurrentUser()` (so called auth. context)
Otherwise most methods would have additional currentUserId parameter.

b) named parameters, for example: `findCarById(id: string)`
- use when there is a **single** parameter

c) domain models, for example: `updateCar(car: Car)`
- use when input is **always, exactly the same** as domain model

d) DTO models, for example: `updateCar(updateCarDto: UpdateCarDto)`
- use when input is **different** than domain model
- use when input can be **changed independently** of the domain model in the future

#### Business service can return values as:
a) nothing, nothing is returned
- use by default for update and delete methods

b) single values, for example: `return 100`
- depends on actual business case

c) domain models, for example: `return car`
- depends on actual business case

d) DTO models, for example: `return updateCarDtoResult`
- use when method output (result) is different than domain model
- use when you need to return a complex object to the client
