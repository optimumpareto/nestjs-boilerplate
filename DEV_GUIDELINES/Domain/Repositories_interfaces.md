# Domain repositories:

**Repository** is responsible for persistance (storage) of the model in selected data source. 

For *each domain model* in our project we need to provide:
- repository interface (a contract of how model will be saved into database and accessed from database)
- specific repository implementation for each database that we use

**Relevant SOLID principles:** 
- Liskov substitution principle 
  *"Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program."*
- Interface segregation principle
  *"Many client-specific interfaces are better than one general-purpose interface."*

---

#### Two types of repositories:

**Generic repositories** (mostly used in our apps):
- Pros: simple to use, consise, generic. 
- Cons: **leaking abstraction** (not all contracted methods will be used for every model type)

Example of generic repository: **IReadDomainRepository**

```typescript
/**
 * A read-only repository for domain objects of type `T`.
 * @typeparam `T` A concrete type of the domain model.
 */
export interface IReadDomainRepository<T> {
  /** Returns all existing objects. */
  findAll(): Promise<Array<T & IBaseModelAttached>>;

  /** Finds an object by ID. Returns an attached object or undefined when object was not found. */
  findById(id: string): Promise<T & IBaseModelAttached | undefined>;

  /** 
   * Finds objects by specified criteria. Returns a list of attached objects. 
   * @param `criteria` A partial object of type `Optional<T>` 
   * containing the list of properties to search by. All properties have been made nullable.
   * 1. All conditions are `AND`ed. To use (OR) logic, run multiple searches.  
   * 2. Use `null`ed property to search by empty (or non-empty) value.
   * 3. Supports the following conditions for primitive types: 
   * `Equals`, `StartsWith`, `EndsWith`, `Contains`, `IsGreaterThan`, `IsLesserThan`
   * and the opposite (`NOT`) conditions. 
   * 4. Supports the following conditions for arrays: 
   * `Equals`, `Contains`, and the opposite (`NOT`) conditions. 
   * 5. Supports nested properties (2nd, 3rd and next level properties). 
   * @param `findOptions` Specifies a matching condition for each property. 
   * The default is: `IsExact`.
   */
  findByCriteria(criteria: Optional<T>, findOptions?: FindOptions<T>): Promise<Array<T & IBaseModelAttached>>;
}
```

NOTE: `find` repository methods always return **attached** models, that is real or default values.

**IWriteDomainRepository**

```typescript
/**
 * A write-only repository for domain objects of type `T`.
 * @typeparam `T` A concrete type of the domain model.
 */
export interface IWriteDomainRepository<T> {
  /** Creates a new object based on detached entity. Returns an attached object. */
  create(object: T): Promise<T & IBaseModelAttached>;

  /** Updates an attached object with a specific ID. */
  update(object: T & IBaseModelAttached): Promise<void>;

  /** Deletes an attached object. */
  delete(object: T & IBaseModelAttached): Promise<void>;
}
```

Explanation as to where we use attached and detached objects:

I. To **create** an object in a database:
- create detached model
- pass it to `create` repository method
- return created object as attached model (with read, unique ID)

II. To **update** a model:
- first always ensure the object exists*
- get attached model from database (i.e. by id)
- change selected attributes of the found model
- pass the modified attached model to `update` repository method

**Even if we update existing object on the front-end side of the system and call API update method - we do not know in the back-end if the object is a real one (the client might have manipulated the request or faked the updated object). There is ALMOST always the need to check the updated object against the database, check the rights of the user, manipulate part of the object, etc. That is why we decided that, to validate front-end data, we will first download object from DB (or back-end cache) and later update it.* 

III. To **delete** a model:
- first always ensure the object exists
- get attached model from database (i.e. by id)
- pass a found model to `delete` repository method

**IDomainRepository combined**

```typescript
/**
 * A read-write repository for domain objects of type `T`.
 * @typeparam `T` A concrete type of the domain model.
 */
export interface IDomainRepository<T> extends
  IReadDomainRepository<T>,
  IWriteDomainRepository<T> {}
```

Example of leaking abstraction: 
a *findById* method in SQL ICarRepository implementation may not work because of limitations of SQL technology (not known at the higher abstraction level). 


**Specific business repositories** (interface contract defines more specific business storage methods):
- Pros: clear, strict, **prevent misuses.**
- Cons: prone to errors, requires much more code.

Example of specific repository:

```typescript
export interface ICarRepository {
    findCarByModel(model: string): Promise<Car[]>;
    findCarByEngineTypeAndMileage(engineType: FuelType, mileage: number): Promise<Car[]>;
    create(object: Car): Promise<Car>;
    updateCarMileage(mileage: number): Promise<void>;
    delete(object: Car): Promise<void>;
}
```

--- 

#### Definition of Matching Condition

```typescript
/** A matching condition for criteria-based search. */
export enum MatchingCondition {
    NOT = -64,
    Equals = 1,
    StartsWith = 2,
    EndsWith = 4, 
    Contains = 8,
    IsGreaterThan = 16,
    IsLesserThan = 32, 
    //computed
    DoesNotEqual = NOT | Equals, 
    DoesNotStartWith = NOT | StartsWith, 
    DoesNotEndWith = NOT | EndsWith, 
    DoesNotContain = NOT | Contains, 
    LesserThanOrEqual = NOT | IsGreaterThan, 
    GreaterThanOrEqual = NOT | IsLesserThan, 
}
```
---

#### Best Practices

- by default let's use generic IDomainRepository<T> for all model types
