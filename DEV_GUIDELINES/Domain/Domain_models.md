# Domain models

Domain Driven Design (DDD) is an approach which focuses on modelling business domain. 

<font color="green">You always start from the domain.</font>

![alt text](../images/Layered_Architecture_Domain.png)

Domain project **must** contain:
- model interfaces

Domain project **may** contain:
- classes with default constructors (simplify creation of objects)
- default models (default data which is not persisted in the database)
- specific repository interfaces (if generic ones are not enough)
- integrity contructor tests

---

#### Domain models:

- represent real business objects.
- encapsulate **consistent state**
- specify the properties that are relevant in current context
- specify the properties you want to manipulate and persist (store in a database).
- are the highest abstraction layer (do not depend on any implementations).

**Relevant SOLID principles**: 
- Dependency inversion principle
  *"One should "depend upon abstractions, [not] concretions."*
- Open–closed principle
  *"Software entities ... should be open for extension, but closed for modification."*

---

#### Best practices for model interfaces

a) **consistency-wise**:
- add `readonly` to those properties which cannot be modified (after creating object)
- use `?` to specify properties that are optional (can be empty)

b) **intention-wise**:
- add type `type IXXXAttached = IXXX & IBaseModelAttached;`
- add doc comments to describe the model, constructor and properties

Example ICar interface: 
```typescript
/** Represents a car. */
export interface ICar {
  /** An immutable reference to manufacturing line. */
  readonly manufacturingLineID?: string;

  /** An immutable model of the car. */
  readonly model: string;

  /** An immutable type of engine. */
  readonly engine: IEngine;

  /** An immutable average fuel consumption. */
  readonly avgFuelConsumption: number;

  /** An immutable full tank capacity. */
  readonly fullTankCapacity: number;

  /** An immutable list of country IDs that this car is produced in. */
  readonly producedIn: string[];

  /** A mutable number of gallons left in the tank. */
  leftGas: number;

  /** A mutable mileage. */
  mileage: number;
}

/** Represents an attached car. */
export type ICarAttached = ICar & IBaseModelAttached;
```

---

#### Best practices for model classes

Model classes are **optional** addons to domain project. 
A class constructor is added to simplify creation of new objects.  

a) **execution-wise**:
- implement the `IXXX` interface
- implement a public constructor

b) **consistency-wise**:
In a constructor:
- pass all required attributes explicitly
- pass all optional properties as `Optional<IXXX>`
- set default values for those optional values that have default values :)

Example Car class: 
```typescript
/** 
 * Implements ICar interface.
 * Adds default constructor which simplifies creation of detached ICar models.
 */
export class Car implements ICar {
  public constructor(
    model: string, 
    engine: IEngine, 
    fullTankCapacity: number, 
    avgFuelConsumption: number, 
    optionalProperties?: Partial<ICar>, 
  ) {
    //required parameters
    this.model = model;
    this.engine = engine;
    this.fullTankCapacity = fullTankCapacity;
    this.avgFuelConsumption = avgFuelConsumption;

    //the parameters which are optional or have default values
    this.manufacturingLineID = optionalProperties?.manufacturingLineID;
    this.mileage = optionalProperties?.mileage || 0;
    this.leftGas = optionalProperties?.leftGas || fullTankCapacity;
    this.producedIn = optionalProperties?.producedIn || [];
  }

  readonly manufacturingLineID?: string;
  readonly model: string;
  readonly engine: IEngine;
  readonly avgFuelConsumption: number;
  readonly fullTankCapacity: number;
  readonly producedIn: string[];

  leftGas: number;
  mileage: number;
}
```

---

#### Attached / detached models

The SOLID principles are used not to protect programmers from mistakes, but to singal an *intent* of the class/function/interface.

**Attached (real) model:** 

- contains unique ID 
- is mostly persisted in a database 
- represents real DB data or simulates it
- can be only created 
when **reading values from database** (`find...` and `create` repository methods) or 
when **simulating real DB data** (defining default values in code)

Each attached model implements the interface:

```typescript
/**
 * A base interface for all attached models.
 */
export interface IBaseModelAttached {
  /**
   * An immutable, unique ID of the object.
   * To prevent future compatility issues, this ID is always of type String.
   */
  readonly id: string;
}
```

To create attached model:
- create **Typescript literal** of IXXXAttached interface
- do not use class constructors

**Detached (floating) model:** 

- does not contain unique ID
- does not have to implement any base interface
- is intended to be inserted into the database
- can be only created 
**in a business service**

To create detached models:
- use **class constructors**
- do not use Typescript literals

---

#### Best practices for default attached data

- default objects simulate real DB data
- each default object must be attached (contain unique id)
- create default objects as **object literals** `{...}`
- when creating a single default object, export this object
- when creating more default objects for a certain collection (i.e. multiple default manufacturing lines) - export the entire collection

For example:

```typescript
const OptimumParetoMLine: IManufacturingLineAttached = {
  id: "OptimumParetoMLine",
  company: "Optimum_Pareto", 
  model: "Optimum_Car", 
  manufacturingLimit: 100
};

const ReasonDrillMLine: IManufacturingLineAttached = {
  id: "ReasonDrillMLine",
  company: "Reason_Drill", 
  model: "RD_Car", 
  manufacturingLimit: 500
};

/** 
 * Represent default manufacturing lines which are not persisted in the database.
 */
export const DefaultManufacturingLines = {
  OptimumParetoMLine,
  ReasonDrillMLine
};
```

Accessing default values when creating detached objects:

```typescript
const car: ICar = new Car(
  "Fiat 500", 
  engine, 
  40, 
  5 / 100, 
  {
    manufacturingLineID: DefaultManufacturingLines.OptimumParetoMLine.id
  });
```

---

#### Reference properties

Model interface can contain primitive types (number, boolean, string, enum) or reference properties (referenced to other domain types). 

There are two ways to store references in object properties:
- as reference to full TS objects
- as string IDs (unique identifiers)

The rule of thumb for embedding (full objects) vs referencing IDs can be 
presented as follows:

![alt text](../images/Embedding_vs_Referencing.png "d")

**Embedding examples:**
- car -> engine (1:1, intrinsic)
- car -> insurance policy documents (1:many, often updated, never queried independently)
- car -> wheels (1:many, rarely updated, rarely queried independently)

Example:

```typescript
export interface ICar {
  //...

  /** An immutable type of engine. */
  readonly engine: IEngine;

  //...
}

/** Represents an engine. */
export interface IEngine {
  /** An immutable model of the engine. */
  readonly model: string;

  /** An immutable type of engine. */
  readonly engineType: FuelType;

  /** An immutable number of horse power. */
  readonly horsePower: number;
}
```

**Referencing examples:**
- rentalAgreement -> carID (1:1, queried independently)
- car -> carCertificateIDs (1:many, queried independently)
- ~~manufacturingline -> carIDs~~ (1:squillion, too many cars per one manufacturing line, requires too many updates)
- car -> manufacturingLineID (squillion:1, parent referencing)

```typescript
export interface ICar {
  //...

  /** An immutable reference to manufacturing line. */
  readonly manufacturingLineID?: string;

  //...
}

/** Represents a detached manufacturing line. */
export interface IManufacturingLine {
  /** An immutable name of the company. */
  readonly company: string;

  /** An immutable model of the manufactured car. */
  readonly model: string;

  /** An immutable limit of the number of cars that can be produced. */
  readonly manufacturingLimit: number;
}
```

---
