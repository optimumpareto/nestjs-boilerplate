# Concrete DB

Concrete DB module is a technological detail. 
It implements repository interfaces from Domain project. 
You should implement **one DB module per each database** you use.

![alt text](./images/Layered_Architecture_DB.png)

Assuming database X, your **DB project will contain**:
- **concrete entities** for database X
- **concrete repositories** for database X
- integration-tests (optional)


## 1. DB Entities:

- for each **model** class defined in the Domain, you should create corresponding **entity** class
- implement BaseEntity and use inheritance

Base Mongo DB entity: 

```typescript
/**
 * A base class for each entity persisted as a collection document in the Mongo DB database.
 */
export class BaseEntity {
  /** A MongoDb ID of the object. */
  @ObjectIdColumn()
  // tslint:disable-next-line: variable-name
  _id: ObjectId;
  
  @Column()
  createdAt: Date;

  @Column()
  lastUpdatedAt: Date;
}
```

Concrete entity:

```typescript
@Entity(CarConsts.CollectionName)
export class CarEntity extends BaseEntity {
  @Column()
  model: string;

  @Column(type => EngineEntity) //sub-document
  engine: EngineEntity;

  @Column()
  mileage: number;

  @Column()
  leftGas: number;

  @Column()
  fullTankCapacity: number;

  @Column()
  avgFuelConsumption: number;

  @Column()
  manufacturingLineID: string; //ref
}
```

Representation of entity is almost completely independent of domain model representation. The general rule is that most property values are mapped one to one. 

For mapping references to other entities you can use the following strategies: 

**1. Domain *refID* -> entity *refID***:
- used in noSQL databases
- when creating attached object, repository copies ID

**2. Domain *refID* -> entity *refObject***:
- used in SQL libraries as foreign key reference
- when creating attached object, repository maps refObject to refID

**3. Domain *refObject* -> entity *refID*** 
- when you want to have full reference in your domain model
- can be used in all types of databases
- when creating attached object, repository maps refID to refObject by downloading related objects from database (potentially expensive action)

**4. Domain *refObject* -> entity *refObject*** 
- when you want to have full reference in your domain model
- used in noSQL as an embedded object (sub-document)
- used in SQL as foreign key reference
- when creating attached object, repository copies and maps the full object

NOTE: Sometimes you cannot fully abstract domain from implementation. 

For example: 

```typescript
@Entity(CarConsts.CollectionName)
export class ExtendedCar extends Car {
  insurance: CarInsurance;
}

export class CarInsurance extends Car {
  legalId: string,
  ...
}

@Entity(CarConsts.CollectionName)
export class MongoDB_ExtendedCarEntity extends CarEntity {
  @Column(type => InsuranceEntity) //sub-document
  insurance: InsuranceEntity;
}

@Entity(CarConsts.CollectionName)
export class SQL_ExtendedCarEntity extends CarEntity {
  @OneToOne(type => InsuranceEntity) //foreign key from separate table
  insurance: InsuranceEntity;
}

const complexCarSearch = async (CarRepository carRepository) => {
  const foundCars = await carRepository.findByCriteria({
    insurance: { legalId: "1234" } //sub-document search?
  });
}
```

NOTE: some repository actions may be very difficult or even impossible to perform on a certain type of database. For example: search by nested property may not be available in SQL database with a foreign key (in this case a JOIN is required). 

## 2. Repositories:

**Relevant SOLID principles:** 
- Liskov substitution principle
   *"Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program."*

ACID: **atomicity**, consistency, **isolation**, **durability**

Implementation of **generic** repository for Mongo DB
```typescript
/**
 * A generic implementation of entity repository.
 * Can be used ONLY when target object does not contain reference objects, but only primitive types.
 * @typeparam `T` is type of domain object.
 * @typeparam `E` is type of DB entity.
 */
export abstract class BaseRepository<T extends BaseModel, E extends BaseEntity>
implements IDomainRepository<T> {
  constructor(private readonly typeOrmDRepository: MongoRepository<E>) {}

  /** Remember about mapping reference properties! */
  protected abstract mapEntityToObject(entity: E): Promise<T>;

  /** Remember about using helper methods from `base.mapper`! */
  protected abstract mapPartialObjectToEntity(object: Optional<T>): Optional<E>;

  ///////////////////////////////////////////////////////////////////////////

  /** Finds all entities of type `E` and maps them to objects of type `T`. */
  async findAll(): Promise<T[]> {
    const allEntities = await this.typeOrmDRepository.find();
    return Promise.all(allEntities.map(e => this.mapEntityToObject(e)));
  }

  /** Finds an entity of type `E` by ID and maps it to object of type `T`. */
  async findById(id: string): Promise<T | undefined> {
    const foundEntity = await this.typeOrmDRepository.findOne(id);
    return foundEntity ? this.mapEntityToObject(foundEntity) : undefined;
  }

  /** Finds entities of type `E` by custom criteria and maps them to objects of type `T`. */
  async findByCriteria(criteria: Optional<T>, findOptions?: FindOptions<T>): Promise<T[]> {
    const mappedProperties = this.mapPartialObjectToEntity(criteria);
    const filteredCriteria = filterPropertiesForSearchCriteria(mappedProperties);

    let mappedCriteria: any = filteredCriteria;
    //if at least one criteria property is an array or 
    //additional options are requested - use complex criteria mapping
    if (findOptions || Object.keys(criteria).find(k => Array.isArray(criteria[k]))) {
      mappedCriteria = mapComplexCriteria(filteredCriteria, findOptions);
    }

    const foundEntities = await this.typeOrmDRepository.find(mappedCriteria);
    return Promise.all(foundEntities.map(e => this.mapEntityToObject(e)));
  }

  /**
   * Takes a new detached object of type `T`, converts it to detached entity of type `E`
   * and inserts it into database.
   */
  async create(object: T): Promise<T> {
    const mappedEntity = this.mapPartialObjectToEntity(object as Optional<T>);
    mappedEntity.createdAt = new Date();
    mappedEntity.lastUpdatedAt = new Date();

    const result = await this.typeOrmDRepository.insertOne(mappedEntity);
    return this.mapEntityToObject(result.ops[0]);
  }

  /** Converts an attached object of type `T` to entity of type `E` and updates it. */
  async update(object: T): Promise<void> {
    const mappedEntity = this.mapPartialObjectToEntity(object as Optional<T>);
    mappedEntity.lastUpdatedAt = new Date();

    const updateResult = await this.typeOrmDRepository.replaceOne({ _id: mappedEntity._id }, mappedEntity);
    return updateResult.modifiedCount === 1 ? Promise.resolve() : Promise.reject();
  }

  /** Deletes an attached object. */
  async delete(object: T): Promise<void> {
    const deleteResult = await this.typeOrmDRepository.deleteOne({ _id: new ObjectId(object.id) });
    return deleteResult.deletedCount === 1 ? Promise.resolve() : Promise.reject();
  }
}
```

Example implementation of concrete repository for Car entities.

```typescript
@Injectable()
export class CarRepository extends BaseRepository<Car, CarEntity>
implements IDomainRepository<Car> {
  constructor(
    @InjectRepository(CarEntity)
    private readonly typeOrmCarRepository: MongoRepository<CarEntity>
  ) {
    super(typeOrmCarRepository);
  }

  protected async mapEntityToObject(entity: CarEntity): Promise<Car> {
    return new Car(entity.model, 
      new Engine(
        entity.engine.model, 
        entity.engine.engineType, 
        entity.engine.horsePower
      ),
      entity.fullTankCapacity, 
      entity.avgFuelConsumption, 
      { 
        manufacturingLineID: entity.manufacturingLineID, 
        mileage: entity.mileage,
        leftGas: entity.leftGas, 
        producedIn: entity.producedIn
      }, 
      mapBasePropertiesForEntity(entity));
  }

  protected mapPartialObjectToEntity(object: Optional<Car>): Optional<CarEntity> {
    return {
      ...mapBasePropertiesForObject(object as Partial<Car>),
      model: object.model, 
      engine: object.engine && {
        model: object.engine?.model, 
        engineType: object.engine?.engineType,
        horsePower: object.engine?.horsePower
      }, 
      fullTankCapacity: object.fullTankCapacity,
      avgFuelConsumption: object.avgFuelConsumption, 
      manufacturingLineID: object.manufacturingLineID, 
      leftGas: object.leftGas, 
      mileage: object.mileage, 
      producedIn: object.producedIn
    };
  }
}
```

**Best Pracices**: 
- for standard persistance functionality, you only need to implement the mapping methods.
- if you need a complex DB logic, create a dedicated repository interface for your model T and implement the repository for each supported database.

### 3. Integration tests

Warning: all tests are run in parallel.
To run them sequentially use `--runInBand` for your jest script in package.json

**Best practices**:
- you only run integration tests on your local machine and test environment
- you should implement repository tests only when including complex logic in the mapping methods, such as downloading related entities